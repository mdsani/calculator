
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>calculator</title>
    <!--bootstrap css-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <style>
        .main{
            width: 300px;
            margin: auto;
            box-shadow: 0px 10px 40px -10px gray;
            display: flex;
            justify-content: center;
            height: 350px;
            margin-top: 100px;
        }
        .icon{
            width: 40px;
            font-size: 20px;
            text-align: start;
        }
        
        h3{
            text-align: center;
            margin-top: 10px;
            margin-bottom: 20px;
        }
        input{
            height: 45px;
            width: 100%;
            outline: none;
            font-size: 16px;
            border-radius: 5px;
            padding-left: 15px;
            border: 1px solid #ccc;
            border-bottom-width: 2px;
            transition: all 0.3s ease;
        }
        .result{
            width: 300px;
            margin: auto;
            box-shadow: 0px 10px 40px -10px gray;
            display: flex;
            align-items: center;
            justify-content: center;
            height: 60px;
        }
    </style>
</head>
<body>

<!--main-->
<div class="main">
    <form action="" method = "post">
        <h3>Calculator</h3>
        <input type= "float" name="num1" placeholder="First number"><br><br>
        <input type="float" name="num2" placeholder="Second number"><br><br>
        <div>
            <div class="d-flex justify-content-center">
                <input type="submit" class="icon"  name="operator" value="+">
            </div>
            <div class="d-flex justify-content-around">
                <input type="submit" class="icon" name="operator" value="-">
                <input type="submit" class="icon" name="operator" value="*">
            </div>
            <div class="d-flex justify-content-center">
                <input type="submit" class="icon" name="operator" value="/">
            </div>
        </div>
    </form>      
</div>


<!--post-->
<div class="result">
<?php

include 'vendor/autoload.php';
 use Sani\Cal;

use function PHPSTORM_META\type;


    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $num1=$_POST['num1'];
        $num2=$_POST['num2'];
        $operator=$_POST['operator'];
        if(is_numeric($num1) && is_numeric($num2)){
            $calcu =new Cal($num1, $num2);
            switch($operator){
                case '+':
                echo 'Result : '.$calcu->add();
                break;
                case '-':
                echo 'Result : '. $calcu->sub();
                break;
                case '/';
                echo 'Result : '. round($calcu->div(),2);
                break;
                case '*';
                echo 'Result : '. $calcu->mul();
                break;
        
            }
        }
    
    }



?>
</div>

<!--bootstrap js-->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>