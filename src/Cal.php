<?php

namespace Sani;

class Cal{
    public $num1;
    public $num2;


    public function __construct($number1,  $number2){
        $this->num1 = $number1;
        $this->num2 = $number2;
    }


    public function add()
    {
     return $this->num1 + $this->num2;
    }
    public function sub()
    {
     return $this->num1 - $this->num2;
    }
    public function div()
    {
     return $this->num1 / $this->num2;
    }
    public function mul()
    {
     return $this->num1 * $this->num2;
    }
    
}
       